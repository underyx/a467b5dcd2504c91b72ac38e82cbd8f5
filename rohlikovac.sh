#!/usr/bin/env bash
curl 'https://www.rohlik.cz/stranka/rohlikovac?do=creditForge-roll' \
  -H 'pragma: no-cache' \
  -H "cookie: $ROHLIK_COOKIES" \
  -H 'accept-encoding: gzip, deflate, br' \
  -H 'accept-language: en-US,en;q=0.9,hu-HU;q=0.8,hu;q=0.7,ja-JP;q=0.6,ja;q=0.5,cs-CZ;q=0.4,cs;q=0.3' \
  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36' \
  -H 'accept: */*' \
  -H 'cache-control: no-cache' \
  -H 'authority: www.rohlik.cz' \
  -H 'x-requested-with: XMLHttpRequest' \
  -H 'referer: https://www.rohlik.cz/stranka/rohlikovac' \
  --compressed